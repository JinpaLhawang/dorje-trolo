/*jshint node:true, es3:false*/
module.exports = function (grunt) {

  // Configuration
  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    // Setup and Takedown
    // Deletes directories
    clean: { before: 'dest', after: 'temp' },

    // Javascript
    // Pre-processes js files for formatting
    jshint: { options: { jshintrc: '.jshintrc' }, gruntfile: { src: 'Gruntfile.js' }, jsFiles: { src: 'src/**/*.js' } },

    // Pre-processes js files for minification (avoids injector errors)
    ngmin: { jsFiles: { expand: true, cwd: 'src/', src: '**/*.js', dest: 'temp/' } },

    // Minifies js files
    uglify: { build: { files: [ { expand: true, cwd: 'temp/', src: '**/*.js', dest: 'dest/', ext: '.min.js' } ] } },

    // CSS
    // Processes less into css files
    less: {
      build: {
        options: { compress: true, },
        files: [ { expand: true, cwd: 'src/', src: '**/*.less', dest: 'dest/', ext: '.css' } ]
      }
    },

    // HTML, Assets and Vendor Libraries
    // Copies assets from src into dest directory
    copy: {
      build: {
        files: [
          { expand: true, cwd: 'src/', src: '**/*.html', dest: 'dest/' },
          { expand: true, cwd: 'src/', src: 'favicon.png', dest: 'dest/' },
          { expand: true, cwd: 'src/images/', src: '**/*', dest: 'dest/images/' },
          { src: 'bower_components/jquery/dist/jquery.min.js', dest: 'dest/vendor/jquery/jquery.min.js' },
          { expand: true, cwd: 'bower_components/bootstrap/dist/', src: '**/*', dest: 'dest/vendor/bootstrap/' },
          { src: 'bower_components/angular/angular.min.js', dest: 'dest/vendor/angular/angular.min.js' },
          {
            src: 'bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
            dest: 'dest/vendor/angular-bootstrap/ui-bootstrap-tpls.min.js'
          },
          {
            src: 'bower_components/angular-ui-router/release/angular-ui-router.min.js',
            dest: 'dest/vendor/angular-ui-router/angular-ui-router.min.js'
          },
          { src: 'bower_components/lodash/dist/lodash.min.js', dest: 'dest/vendor/lodash/lodash.min.js' }
        ]
      }
    },

    // Testing
    // Runs unit tests
    karma: { unit: { configFile: 'karma.conf.js', background: true } },

    // Deployment
    // Creates web server
    connect: {
      server: { options: { port: 5678, base: 'dest', protocol: 'http', hostname: 'dorje-trolo.sandbox.local' } }
    },

    // Opens browser to web server
    open: {
      build: {
        path: '<%= connect.server.options.protocol %>://<%= connect.server.options.hostname %>:<%= connect.server.options.port %>/'
      }
    },

    // Watches for file changes and reruns tasks
    watch: { scripts: { files: 'src/**/*', tasks: [ 'refresh' ], options: { livereload: true } } }
  });

  // Load Plugins
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-ngmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-karma');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-open');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Register Tasks
  grunt.registerTask('setup', [ 'karma:unit', 'clean:before' ]); // Start karma server and delete dist directory
  grunt.registerTask('buildScripts', [ 'jshint', 'ngmin', 'uglify' ]); // Prepare js files
  grunt.registerTask('build', [ 'buildScripts', 'less', 'copy' ]); // Prepare build in dist directory
  grunt.registerTask('takedown', [ 'clean:after' ]); // Delete temp directory used in preparing js files
  grunt.registerTask('deploy', [ 'connect', 'open', 'watch' ]); // Start web server, open browser and watch files

  grunt.registerTask('refresh', [ 'clean:before', 'build', 'takedown', 'karma:unit:run' ]); // Refeshes deployment

  grunt.registerTask('default', [ 'setup', 'build', 'takedown', 'deploy' ]);
};
