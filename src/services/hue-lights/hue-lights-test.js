/* globals describe, beforeEach, module, inject, it, expect */
describe('api.hueLights', function () {

  beforeEach(module('api.hueLights'));
  beforeEach(module('api.hueLights.lights.mock'));

  var $httpBackend, mockResponse;

  beforeEach(inject(function ($injector) {
    $httpBackend = $injector.get('$httpBackend');
    mockResponse = $injector.get('LIGHTS_MOCK');
  }));

  describe('hueLightsService', function () {

    describe('#getLights()', function () {

      it('should return lights', inject(function (hueLightsService) {

        $httpBackend.whenGET(hueLightsService.getApiUrl()).respond(mockResponse);

        var lights = {};

        hueLightsService.getLights().then(function (resp) {
          lights = resp;
        });
        $httpBackend.flush();

        expect(lights['1'].name).toBe('Hue Lamp 1');
        expect(lights['2'].name).toBe('Hue Lamp 2');
        expect(lights['3'].name).toBe('Hue Lamp 3');
      }));
    });
  });
});
