angular.module('api.hueLights', [])

.constant('PROTOCOL', 'http://')
.constant('API_PATH', '/api/')
.constant('UNAUTH_USER_ERROR_TYPE', 1)
.constant('UNAUTHORIZED_STATUS', 'UNAUTHORIZED')

.service('hueLightsService', function (
    $http,
    $q,
    PROTOCOL,
    API_PATH,
    UNAUTH_USER_ERROR_TYPE,
    UNAUTHORIZED_STATUS
  ) {

  var hueBridgeIpAddress, hueBridgeUsername;

  var hueLightsService = {

    getApiUrl: function () {
      return PROTOCOL + hueBridgeIpAddress + API_PATH + hueBridgeUsername + '/lights';
    },

    setHueBridgeIpAddress: function (ipAddress) {
      hueBridgeIpAddress = ipAddress;
    },

    setHueBridgeUsername: function (username) {
      hueBridgeUsername = username;
    },

    // GET LIGHTS
    getLights: function () {

      return $http.get(this.getApiUrl()).then(function (resp) {
        if (_.isArray(resp.data) && !_.isEmpty(resp.data) && !_.isUndefined(resp.data[0].error)) {
          var err = {};
          if (resp.data[0].error.type === UNAUTH_USER_ERROR_TYPE) {
            err.status = UNAUTHORIZED_STATUS;
          } else {
            err.response = resp.data[0].error;
          }
          return $q.reject(err);
        }
        return resp.data;
      }, function (err) {
        return $q.reject(err);
      });
    },

    // GET LIGHT
    getLight: function (id) {

      var url = this.getApiUrl() + '/' + id;

      return $http.get(url).then(function (resp) {
        return resp.data;
      });
    },

    // SET LIGHT
    setLight: function (id, params) {

      var url = this.getApiUrl() + '/' + id + '/state';

      return $http.put(url, params);
    }
  };

  return hueLightsService;
});
