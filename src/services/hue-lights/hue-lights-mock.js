angular.module('api.hueLights.lights.mock', [])

.constant('LIGHTS_MOCK', {
  '1': {
    'name': 'Hue Lamp 1'
  },
  '2': {
    'name': 'Hue Lamp 2'
  },
  '3': {
    'name': 'Hue Lamp 3'
  }
})

.constant('LIGHT_ON_MOCK', {
  'state': {
    'on': true,
    'bri': 122,
    'hue': 24340,
    'sat': 254,
    'xy': [ 0.4221, 0.5084 ],
    'ct': 310,
    'alert': 'none',
    'effect': 'none',
    'colormode': 'hs',
    'reachable': true
  },
  'type': 'Extended color light',
  'name': 'Hue Lamp 1',
  'modelid': 'LCT001',
  'swversion': '66010820',
  'pointsymbol': {
    '1': 'none',
    '2': 'none',
    '3': 'none',
    '4': 'none',
    '5': 'none',
    '6': 'none',
    '7': 'none',
    '8': 'none'
  }
})

.constant('LIGHT_OFF_MOCK', {
  'state': {
    'on': false,
    'bri': 122,
    'hue': 40103,
    'sat': 254,
    'xy': [ 0.2458, 0.1956 ],
    'ct': 500,
    'alert': 'none',
    'effect': 'none',
    'colormode': 'hs',
    'reachable': true
  },
  'type': 'Extended color light',
  'name': 'Hue Lamp 2',
  'modelid': 'LCT001',
  'swversion': '66010820',
  'pointsymbol': {
    '1': 'none',
    '2': 'none',
    '3': 'none',
    '4': 'none',
    '5': 'none',
    '6': 'none',
    '7': 'none',
    '8': 'none'
  }
});
