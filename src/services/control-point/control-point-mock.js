angular.module('api.controlPoint.mock', [])

.constant('GET_INTERNET_GATEWAY_SUCCESS', {
  'uuid': '824ff22b-8c7d-41c5-a131-44f534e125551',
  'ipAddress': '10.0.0.1',
  'port': '5000',
  'deviceType': 'urn:schemas-upnp-org:device:InternetGatewayDevice:1',
  'modelName': 'ARRIS TG862 Router',
  'friendlyName': 'ARRIS TG862 Router',
  'binaryState': null
});
