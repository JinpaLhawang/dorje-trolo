angular.module('api.controlPoint', [])

.constant('CONTROL_POINT_API', 'http://10.0.0.12:6767/api/control_point')
//.constant('CONTROL_POINT_API', 'http://dorje-trolo.sandbox.local:6767/api/control_point')

.service('controlPointService', function (
    $http,
    $q,
    CONTROL_POINT_API
  ) {

  var controlPointService = {

    getApiUrl: function () {
      return CONTROL_POINT_API;
    },

    // GET CONTROL POINT
    get: function () {

      return $http.get(this.getApiUrl()).then(function (resp) {
        return resp.data;
      });
    },

    // GET INTERNET GATEWAY
    getInternetGateway: function () {

      var url = this.getApiUrl() + '/gateway';

      return $http.get(url).then(function (resp) {
        return resp.data;
      }, function (err) {
        return $q.reject(err);
      });
    },

    // GET HUE BRIDGE
    getHueBridge: function () {

      var url = this.getApiUrl() + '/hue_bridge';

      return $http.get(url).then(function (resp) {
        return resp.data;
      }, function (err) {
        return $q.reject(err);
      });
    },

    // GET WEMOS
    getWeMos: function () {

      var url = this.getApiUrl() + '/wemos';

      return $http.get(url).then(function (resp) {
        return resp.data;
      }, function (err) {
        return $q.reject(err);
      });
    },

    // GET DEVICE
    getDevice: function (uuid) {

      var url = this.getApiUrl() + '/devices/' + uuid;

      return $http.get(url).then(function (resp) {
        return resp.data;
      });
    },

    // SET DEVICE BINARY STATE
    setDeviceBinaryState: function (uuid, binaryState) {

      var url = this.getApiUrl() + '/devices/set_binary_state/' + uuid;
      var params = { binaryState: binaryState };

      return $http.put(url, params).then(function (resp) {
        return resp.data;
      });
    }
  };
  
  return controlPointService;
});
