/* globals describe, beforeEach, module, inject, it, expect */
describe('Control Point', function () {

  beforeEach(module('api.controlPoint'));
  beforeEach(module('api.controlPoint.mock'));

  var $httpBackend, mockResponse;

  beforeEach(inject(function ($injector) {
    $httpBackend = $injector.get('$httpBackend');
    mockResponse = $injector.get('GET_INTERNET_GATEWAY_SUCCESS');
  }));

  it('should return an internet gateway', inject(function (controlPointService) {

    $httpBackend.whenGET(controlPointService.getApiUrl() + '/gateway').respond(mockResponse);

    var internetGateway = {};

    controlPointService.getInternetGateway().then(function (resp) {
      internetGateway = resp;
    });
    $httpBackend.flush();

    expect(internetGateway.uuid).toBe('824ff22b-8c7d-41c5-a131-44f534e125551');
    expect(internetGateway.ipAddress).toBe('10.0.0.1');
    expect(internetGateway.port).toBe('5000');
    expect(internetGateway.deviceType).toBe('urn:schemas-upnp-org:device:InternetGatewayDevice:1');
    expect(internetGateway.modelName).toBe('ARRIS TG862 Router');
    expect(internetGateway.friendlyName).toBe('ARRIS TG862 Router');
    expect(internetGateway.binaryState).toBe(null);
  }));
});
