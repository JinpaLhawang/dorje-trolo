angular.module('hueLights', [
  'ui.bootstrap',
  'api.controlPoint',
  'api.hueLights',
  'model.hueLights.light'
])

.constant('HUE_BRIDGE_USERNAME', 'newdeveloper')

.constant('UNAUTHORIZED_STATUS', 'UNAUTHORIZED')

.constant('LOAD_HUE_BRIDGE_ERROR_ALERT', {
  type: 'danger',
  msg: 'There was an error loading the Hue Bridge or none could be found.'
})

.constant('LOAD_LIGHTS_UNAUTHORIZED_ALERT', {
  type: 'danger',
  msg: 'The user is not authorized to load lights.'
})
.constant('LOAD_LIGHTS_ERROR_ALERT', {
  type: 'danger',
  msg: 'There was an error loading the lights or no lights could be found.'
})

.controller('HueLightsCtrl', function (
    $scope,
    controlPointService,
    hueLightsService,
    hueLightFactory,
    LOAD_HUE_BRIDGE_ERROR_ALERT,
    HUE_BRIDGE_USERNAME,
    UNAUTHORIZED_STATUS,
    LOAD_LIGHTS_UNAUTHORIZED_ALERT,
    LOAD_LIGHTS_ERROR_ALERT
  ) {

  $scope.hueBridge = {};
  $scope.isHueBridgeLoading = false;

  $scope.username = '';
  $scope.username = HUE_BRIDGE_USERNAME;

  $scope.lights = {};
  $scope.isLightsLoading = false;
  $scope.alerts = [];

  // HUE BRIDGE
  function hueBridgeLoaded(resp) {
    $scope.isHueBridgeLoading = false;
    $scope.hueBridge = resp;
    if (!_.isEmpty($scope.username)) {
      $scope.loadLights();
    }
  }

  function loadHueBridgeError() {
    $scope.isHueBridgeLoading = false;
    _.unique($scope.alerts.push(LOAD_HUE_BRIDGE_ERROR_ALERT));
  }

  $scope.getHueBridge = function () {
    $scope.isHueBridgeLoading = true;
    controlPointService.getHueBridge().then(hueBridgeLoaded, loadHueBridgeError);
  };

  $scope.isHueBridgeLoaded = function () {
    return !_.isEmpty($scope.hueBridge);
  };

  // USERNAME
  $scope.submitUsername = function (username) {
    $scope.username = username;
    $scope.loadLights();
  };

  // LIGHTS
  function getLight(id) {
    hueLightsService.getLight(id).then(function (resp) {
      $scope.lights[id] = hueLightFactory.create(resp);
      $scope.lightsOn = !_.isUndefined(_.find($scope.lights, function (light) {
        return light.isOn();
      }));
    });
  }

  function lightsLoaded(resp) {
    $scope.isLightsLoading = false;
    $scope.lights = resp;
    _.forEach(resp, function (light, id) {
      getLight(id);
    });
  }

  function loadLightsError(err) {
    $scope.isLightsLoading = false;
    if (err.status === UNAUTHORIZED_STATUS) {
      _.unique($scope.alerts.push(LOAD_LIGHTS_UNAUTHORIZED_ALERT));
    } else {
      _.unique($scope.alerts.push(LOAD_LIGHTS_ERROR_ALERT));
    }
  }

  $scope.loadLights = function () {
    $scope.isLightsLoading = true;
    hueLightsService.setHueBridgeIpAddress($scope.hueBridge.ipAddress);
    hueLightsService.setHueBridgeUsername($scope.username);
    hueLightsService.getLights().then(lightsLoaded, loadLightsError);
  };

  $scope.isLightsLoaded = function () {
    return !_.isEmpty($scope.lights);
  };

  // COMMANDS
  $scope.toggleLight = function (id, on) {
    var params = { 'on': on };
    hueLightsService.setLight(id, params).then(function () {
      getLight(id);
    });
  };

  $scope.toggleLights = function (on) {
    _.forEach($scope.lights, function (light, id) {
      $scope.toggleLight(id, on);
    });
  };

  $scope.randomizeLightColors = function () {
    var params;
    _.forEach($scope.lights, function (light, id) {
      params = { 'on': true, 'sat': 254, 'bri': 122, 'hue': _.random(0, 65535) };
      hueLightsService.setLight(id, params).then(function () {
        getLight(id);
      });
    });
  };

  $scope.colorRandomizerOn = false;
  var timer;
  $scope.toggleColorRandomizer = function (state) {
    $scope.colorRandomizerOn = state;

    if ($scope.colorRandomizerOn) {
      timer = setInterval($scope.randomizeLightColors, 60000);
    } else {
      clearInterval(timer);
    }
  };

  $scope.getHueBridge();
});
