angular.module('internetGateway', [
  'ui.bootstrap',
  'api.controlPoint'
])

.constant('LOAD_INTERNET_GATEWAY_ERROR_ALERT', {
  type: 'danger',
  msg: 'There was an error loading the Internet Gateway or none could be found.'
})

.controller('InternetGatewayCtrl', function (
    $scope,
    controlPointService,
    LOAD_INTERNET_GATEWAY_ERROR_ALERT
  ) {

  $scope.internetGateway = {};
  $scope.isLoading = false;
  $scope.alerts = [];

  function internetGatewayLoaded(resp) {
    $scope.isLoading = false;
    $scope.internetGateway = resp;
  }

  function loadInternetGatewayError() {
    $scope.isLoading = false;
    _.unique($scope.alerts.push(LOAD_INTERNET_GATEWAY_ERROR_ALERT));
  }

  $scope.getInternetGateway = function () {
    $scope.isLoading = true;
    controlPointService.getInternetGateway().then(internetGatewayLoaded, loadInternetGatewayError);
  };

  $scope.isLoaded = function () {
    return !_.isEmpty($scope.internetGateway);
  };

  $scope.getInternetGateway();
});
