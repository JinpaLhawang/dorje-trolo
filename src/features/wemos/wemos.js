angular.module('wemos', [
  'ui.bootstrap',
  'api.controlPoint'
])

.constant('LOAD_WEMOS_ERROR_ALERT', {
  type: 'danger',
  msg: 'There was an error loading the WeMos or none could be found.'
})

.controller('WeMosCtrl', function (
    $scope,
    controlPointService,
    LOAD_WEMOS_ERROR_ALERT
  ) {

  $scope.wemos = [];
  $scope.isLoading = false;
  $scope.alerts = [];

  function wemosLoaded(resp) {
    $scope.isLoading = false;
    $scope.wemos = resp;
  }

  function loadWeMosError() {
    $scope.isLoading = false;
    _.unique($scope.alerts.push(LOAD_WEMOS_ERROR_ALERT));
  }

  $scope.getWeMos = function () {
    controlPointService.getWeMos().then(wemosLoaded, loadWeMosError);
  };

  $scope.getWeMo = function (index) {
    var uuid = $scope.wemos[index].uuid;
    controlPointService.getDevice(uuid).then(function (resp) {
      $scope.wemos[index] = resp;
    });
  };

  $scope.setWeMoBinaryState = function (index, binaryState) {
    var uuid = $scope.wemos[index].uuid;
    controlPointService.setDeviceBinaryState(uuid, binaryState).then(function (resp) {
      $scope.wemos[index] = resp;
    });
  };

  $scope.isLoaded = function () {
    return !_.isEmpty($scope.wemos);
  };

  $scope.getWeMos();
});
