/* globals describe, beforeEach, module, inject, it, expect */
describe('model.hueLights.light', function () {

  beforeEach(module('model.hueLights.light'));
  beforeEach(module('api.hueLights.lights.mock'));

  var LIGHT_ON_DATA, LIGHT_OFF_DATA;
  var hueLightFactory;
  var hueLightOn, hueLightOff;

  beforeEach(inject(function ($injector) {
    LIGHT_ON_DATA = $injector.get('LIGHT_ON_MOCK');
    LIGHT_OFF_DATA = $injector.get('LIGHT_OFF_MOCK');
    hueLightFactory = $injector.get('hueLightFactory');
    hueLightOn = hueLightFactory.create(LIGHT_ON_DATA);
    hueLightOff = hueLightFactory.create(LIGHT_OFF_DATA);
  }));

  describe('hueLightFactory', function () {

    describe('#create()', function () {

      it('should return an instance of the hueLight model', inject(function (hueLightFactory) {
        expect(hueLightOn instanceof hueLightFactory.HueLight).toBe(true);
      }));
    });

    describe('#isOn()', function () {

      it('should return true if the light is on', function () {
        expect(hueLightOn.isOn()).toBe(true);
      });

      it('should return false if the light is off', function () {
        expect(hueLightOff.isOn()).toBe(false);
      });
    });
  });
});
