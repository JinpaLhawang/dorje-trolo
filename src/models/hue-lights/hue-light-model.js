angular.module('model.hueLights.light', [])

.factory('hueLightFactory', function () {

  function HueLight(hueLight) {
    _.extend(this, hueLight);
  }

  _.extend(HueLight.prototype, {

    isOn: function () {
      return !_.isUndefined(this.state) && this.state.on;
    }
  });

  return {
    create: function (hueLight) {
      return new HueLight(hueLight);
    },
    HueLight: HueLight
  };
});
