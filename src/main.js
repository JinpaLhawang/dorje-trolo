angular.module('dashboard', [
  'ui.bootstrap',
  'ui.router',
  'menu',
  'hueLights',
  'wemos',
  'internetGateway'
])

.config(function (
    $stateProvider
  ) {

  $stateProvider.state('dashboard', {
    views: {
      'menu': {
        templateUrl: '/features/menu/templates/menu-tmpl.html'
      },
      'hueLights': {
        templateUrl: '/features/hue-lights/templates/hue-lights-tmpl.html'
      },
      'wemos': {
        templateUrl: '/features/wemos/templates/wemos-tmpl.html'
      },
      'internetGateway': {
        templateUrl: '/features/internet-gateway/templates/internet-gateway-tmpl.html'
      }
    }
  });
})

.controller('DashboardCtrl', function (
    $scope,
    $state
  ) {

  $scope.showMenu = false;

  $state.transitionTo('dashboard');
});
